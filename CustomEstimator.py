import tensorflow as tf
import  numpy as np

## y= Wx + b

# features (values of x)
# labels (expected values of y for values of x)
# we dont have to worry too much about the model right now



## please note that creating a estimator is a little different than creating a custom model like we did earlier
## so there may be a few changes in the way we use to do things earlier


def model_fn(features,labels,mode):

    ## when we are in predict phase (mode = infer) then labels will come as noe because we haev to predict them
    ## so we will use an if condition to check if model = 'infer' i.e predict then we will asign some default value to it.

    if mode == 'infer':
        labels = np.array([0,0])


    ## we create variable nodes like this
    ## here we have to use flot64 insted of float 32 (default for estimator)
    W = tf.get_variable(name='W',shape=[1],dtype=tf.float64)
    b = tf.get_variable(name='b', shape=[1], dtype=tf.float64)

    # we are fetching the values of x which user has given in features dictionary
    y = W * features['x'] + b

    ## calculating loss is same as we use to do in creating custom model
    loss = tf.reduce_sum(input_tensor=tf.square(x=(y-labels)))
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)


    ## this setp is litle different in estimator
    ## we will take initial value of global steps and then we will add 1 to it to increase the count

    global_step = tf.train.get_global_step()
    train_step = tf.group(optimizer.minimize(loss=loss),tf.assign_add(global_step,1))

    return tf.estimator.EstimatorSpec(
        mode = mode,
        predictions=y,
        loss=loss,
        train_op=train_step
    )


### now we will use this model in below code



x_train = np.array([1.0,2.0,3.0,4.0])
y_train = np.array([-1.0,-2.0,-3.0,-4.0])
x_evel = np.array([5.0,10.0,15.0,20.0])
y_evel = np.array([-5.0,-10.0,-15.0,-20.0])
x_predict = np.array([50.0,100.0])




## here we will use our own estimator instead of uisng the pre built one



feature_column = tf.feature_column.numeric_column(key='x',shape=[1])
feature_columns = [feature_column]


estimator  = tf.estimator.Estimator(model_fn=model_fn)

#estimator = tf.estimator.LinearRegressor(feature_columns= feature_columns)






inp_func = tf.estimator.inputs.numpy_input_fn(x = {'x':x_train}, # for all out inp we have to specify a value in dictionary (here we have only 'x' as inp so there is only one value in dict)
                                   y = y_train,  ## expected output
                                   batch_size=4, ## no of items in a batch (size of list inp)
                                    num_epochs=None, ## later
                                   shuffle=True        ## later
                                   )


train_inp_func = tf.estimator.inputs.numpy_input_fn(x = {'x':x_train}, # for all out inp we have to specify a value in dictionary (here we have only 'x' as inp so there is only one value in dict)
                                   y = y_train,  ## expected output
                                   batch_size=4, ## no of items in a batch (size of list inp)
                                    num_epochs=1000, ## later
                                   shuffle=False        ## later
                                   )

evel_inp_func = tf.estimator.inputs.numpy_input_fn(x = {'x':x_evel}, # for all out inp we have to specify a value in dictionary (here we have only 'x' as inp so there is only one value in dict)
                                   y = y_evel,  ## expected outputoutput
                                   batch_size=4, ## no of items in a batch (size of list inp)
                                    num_epochs=1000, ## later
                                   shuffle=False        ## later
                                   )


estimator.train(input_fn=inp_func,steps=1000)

print("Training Phase on Training Data     : "+str(estimator.evaluate(input_fn=train_inp_func)))

print("Evaluating model uisng testing data : "+str(estimator.evaluate(input_fn=evel_inp_func)))



predict_inp_func = tf.estimator.inputs.numpy_input_fn(x = {'x':x_predict}, # for all out inp we have to specify a value in dictionary (here we have only 'x' as inp so there is only one value in dict)
                                    num_epochs=1, ## later
                                   shuffle=False        ## later
                                   )




myPredict = estimator.predict(input_fn=predict_inp_func)
print(list(myPredict))


