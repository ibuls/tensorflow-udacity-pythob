import tensorflow as tf
import numpy as np

## Estimators
'''
Estimators make things a lot easy then creating a computation graph ourselves.
It simplifies the intire process of building,training,testing and evaluating process
There is no need for graph construction, simply specity the input values,input functions
It is a bit more restrictive then building a graph ourselves and is pirticular about inputs
Inputs are called as features.

'''



## we will create a linear regression model using estimator

x_train = [1.0,2.0,3.0,4.0]
y_train = [-1.0,-2.0,-3.0,-4.0]

x_evel = [5.0,10.0,15.0,20.0]
y_evel = [-5.0,-10.0,-15.0,-20.0]


## since we have only 1 feature column in our exuation y = Wx+b i.e 'x'
## so we need only 1 feature

## key='x' means we will pe specifying the values to x in a linear regression model

## here we choose numeric_clounm because of aur float case

feature_column = tf.feature_column.numeric_column(key='x',shape=[1])
feature_columns = [feature_column]


## estimator specifies which function we are using
estimator  = tf.estimator.LinearRegressor(feature_columns = feature_columns)


##note estimart does not accept any array
## it uses only numpy array

x_train = np.array(x_train)
y_train = np.array(y_train)
x_evel = np.array(x_evel)
y_evel = np.array(y_evel)



## trains the model
## input function for training the model uisng training data
inp_func = tf.estimator.inputs.numpy_input_fn(x = {'x':x_train}, # for all out inp we have to specify a value in dictionary (here we have only 'x' as inp so there is only one value in dict)
                                   y = y_train,  ## expected output
                                   batch_size=4, ## no of items in a batch (size of list inp)
                                    num_epochs=None, ## later
                                   shuffle=True        ## later
                                   )



## input func for evaluating the training phase using training data
## evaluates model in training phase itsef
train_inp_func = tf.estimator.inputs.numpy_input_fn(x = {'x':x_train}, # for all out inp we have to specify a value in dictionary (here we have only 'x' as inp so there is only one value in dict)
                                   y = y_train,  ## expected output
                                   batch_size=4, ## no of items in a batch (size of list inp)
                                    num_epochs=1000, ## later
                                   shuffle=False        ## later
                                   )

## inp function for testing the model using testing data
## evaluates model after training is done
evel_inp_func = tf.estimator.inputs.numpy_input_fn(x = {'x':x_evel}, # for all out inp we have to specify a value in dictionary (here we have only 'x' as inp so there is only one value in dict)
                                   y = y_evel,  ## expected outputoutput
                                   batch_size=4, ## no of items in a batch (size of list inp)
                                    num_epochs=1000, ## later
                                   shuffle=False        ## later
                                   )


## run the training phase with 1000 epochs and the training function.
estimator.train(input_fn=inp_func,steps=1000)

## now we have to evalue how well our model did

print("Training Phase on Training Data     : "+str(estimator.evaluate(input_fn=train_inp_func)))
print("Evaluating model uisng testing data : "+str(estimator.evaluate(input_fn=evel_inp_func)))


## input function for prediction

## we will predict
x_predict = np.array([50.0,100.0])

predict_inp_func = tf.estimator.inputs.numpy_input_fn(x = {'x':x_predict}, # for all out inp we have to specify a value in dictionary (here we have only 'x' as inp so there is only one value in dict)
                                    num_epochs=1, ## later
                                   shuffle=False        ## later
                                   )
myPredict = estimator.predict(input_fn=predict_inp_func)
print(list(myPredict))