import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

mnist_data = input_data.read_data_sets('MNIST_data', one_hot=True)

## one hot means we will write 2 as 010000000 as 2nd pos is 1

# train_image = mnist_data.train.images[0] ## images contain 60,000 images we are taking only 1st one
# train_label = mnist_data.train.labels[0]
## the given image os of 7  hence we get [0. 0. 0. 0. 0. 0. 0. 1. 0. 0.]
# print(train_image)
# print(train_label)

## images which are part of examples are already in the required format (lables are also roperlt given hence we dont have to filter it)


## we will use linear regression for this Simple MNIST
## i.e  y = Wx + b
# here y is the label (0 to 9) and x is the image


x_input = tf.placeholder(dtype=tf.float32,
                         shape=[None, 784],  # None for number of images (can be 1 to 60,000 | 784 because 28*28 = 784
                         name='x_input')

W = tf.Variable(initial_value=tf.zeros(shape=[784, 10]), name='W')
## we are providing initial value of all zeros
## 784 because 28*28 = 784 values
## 10 becuase we have to o/p an array of length 10 eg : [00001000000] = 4 (1 at 4th position0)0
## we can also read above line as matching each of 784 elements to one of 10 elements (0 to 9)

b = tf.Variable(initial_value=tf.zeros(shape=[10]), name='b')
## this is only going to be an array of 10 elements
## because ofuput from W will be an array of 10 elements (to add we have to have shame shape)


##N(RXC)
## A(1x5) * B(5X1) = C(1X1)

y_actual = tf.add(x=tf.matmul(a=x_input,
                              b=W,
                              name='matmul')
                  , y=b,
                  name="y_actual")

y_expcted = tf.placeholder(dtype=tf.float32, shape=[None, 10], name='y_expected')

## Now we have created basic graph setup


cross_entropy_loss = tf.reduce_mean(input_tensor=tf.nn.softmax_cross_entropy_with_logits(
    labels=y_expcted, ## values we are expecting
    logits=y_actual  ## values which are actually coming
),
    name='cross_entropy_loss')

## we use softmax becuase thee is probablity involved (confidence % involved)

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.05,name='optimizer')
train_step = optimizer.minimize(
    loss=cross_entropy_loss,
    name='train_step'
)