Hey!, this entire project is based upon a udemy course called "Python Master class"

the main aim of this project is

1) Learn Tensorflow
2) Export models which are made in tensorflow to Android Studios


Index Of Course:

1) Constant Nodes, Sessions : UdemyTesnorflow1.py
2) Variable Nodes : UdemyTesnorflow2.py
3) Placeholder Node : UdemyTesnorflow3.py
4) Operation Node : UdemyTesnorflow4.py
5) Loss function and optimizers : UdemyTesnorflow4.py
6) A Simple Linear Regression Model : Linear_Regression_Model.py 
7) Introduction to estimator : IntroToEstimator.py
8) Creating a Custom Estimator: CustomEstimator.py
9) Saving LinearRegression Model to Protobuff file: LR_Model_To_Protobuff.py
10) Optimized Graph: Freeze_Graph.py



