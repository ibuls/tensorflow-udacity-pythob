import tensorflow as tf


## now we will actually create a linear regression model

## y= Wx + b note-> here W denotes Weights can be interchanged with M(same thing)



# creating required variables
from numpy.core.multiarray import dtype

W = tf.Variable(initial_value=[1.0],dtype=tf.float32)
b= tf.Variable(initial_value=[1.0],dtype=tf.float32)

x = tf.placeholder(dtype=tf.float32)


# y_input will be used under training
# y_input will be used for feeding the correct answers
# during the training
y_input = tf.placeholder(dtype=tf.float32)


## to reduce time we are using the shortcut
## but try to use the traiditonal way
y_output = W * x + b



## three basic steps of
## loss function
## optimizer

loss = tf.reduce_sum(input_tensor=tf.square(x=y_output-y_input))
## if the learning rate is small then our
## result will be more precise, but it will take more time
## as we are chaing the value little by little (0.01 in our case below)
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01)
## if our learning_rate is large then training will be over really fast
## but in this case we will loose the precisenesss of the actuall outcome

train_setp =  optimizer.minimize(loss=loss)

session = tf.Session()
session.run(tf.global_variables_initializer())
## remember in variables we  have to run the global variable initializer
## before fething the output

## now we need input data and expected output data(x is out inp and y will be our output)
x_train = [1.0,2.0,3.0,4.0]
# for expeted output we will simply multiply the input by -1
y_train  = [-1.0,-2.0,-3.0,-4.0]


## at each iteration of training values of W and b will be changed by 0.01
## as we have set our learning rate as 0.01
## the process will be repeted util we minimise the loss function (as much as we can)

out_before_training = session.run(loss,feed_dict={x:x_train,y_input:y_train})
print("before training output : "+str(out_before_training))
print("\n\n")
## this loop will run 1000 times to train our model on out input data
for _ in range(1000):
    session.run(fetches=train_setp,feed_dict={x:x_train,y_input:y_train})

out_after_training = session.run(fetches=[loss,W,b],feed_dict={x:x_train,y_input:y_train})
print("after training output : "+str(out_after_training))
print("\n\n")
testing_model = session.run(fetches=y_output,feed_dict={x:[5.0,10.0,15.0]})
print("Testing Model output : "+str(testing_model))






