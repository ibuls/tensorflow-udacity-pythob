import tensorflow as tf


## Placeholder node
## Place hoder nodes are not assigned values when they are created but at runtime
## the nodes act as starting point
## they provide input to the graph
## hence they are the first nodes in the graph.

placeholder1 = tf.placeholder(dtype=tf.float32,
                             shape=(1,4),  # not compulsory but it is used for checking that our input is correct
                                          # eg : if we are training a model on 25X25 image then shape will be 625
                                        # or we can alos specfy list dimensions eg : (1,4)
                             name="placeholder1"
                             )


placeholder2 = tf.placeholder(dtype=tf.float32,
                             shape=(2,2),  # not compulsory but it is used for checking that our input is correct
                                          # eg : if we are training a model on 25X25 image then shape will be 625
                                        # or we can alos specfy list dimensions eg : (1,4)
                             name="placeholder2"
                             )


# running is same as all other nodes using a session

session  = tf.Session()


output = session.run(fetches=[placeholder1,placeholder2],
                     feed_dict={
                                    placeholder1:[[1.0,2.0,3.0,4.0]],
                                    placeholder2:[[5.0,6.0],[7.0,8.0]]
                                }
                     )

print("Output : "+str(output))
# here fetches specifies the nodes whose values we want to observe
# feed_dict specifies the input to the nodes which we have given



