import tensorflow as tf


## here we will discuss about
## 1) Loss Fucntion
## 2) Optimizers


## 1) Loss Function
## loss function defines the difference b/w actual value and expected value
## actual value : output from model given and input
## expected value: correct o/p given an input

## 2) Optimizers
## changes values in model to alter loss(typically to minimize).
## values altered during testing phase
## models acessed during testing


inpList = [1.0,2.0,3.0,4.0]
expList = [2.0,3.0,4.0,5.0]
actList = [1.5,2.5,3.5,4.5]

loss = tf.reduce_sum(input_tensor=tf.square(x=expList-actList))
## the above line will take the difference of exp-act
## squre ther result
## add all the values and put it to loss variable

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.05)
train_step = optimizer.minimize(loss)

## Please note
## the selection of Loss function and
## the optimizer is sort of an art
## we have to see what data we have and for that what optimizer we have to choose




