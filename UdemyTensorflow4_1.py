import tensorflow as tf

## in this code we will mimic the
## Linear Regression i.e y=Mx+C
## here M and C are constans and x is the variable
## so we will use "Constant node" for M and C
## and a "Placeholder" node for x

M = tf.constant(value=[1.0])
C = tf.constant(value=[2.0])
x = tf.placeholder(dtype=tf.float32)

## now to write above equation in shortcut we can simply use

#y = M*x+C

# or we can use the good old way

mult = tf.multiply(x=M,y=x,name="Mx")
y = tf.add(x=mult,y=C,name="out")

## and then print the output using session
session  = tf.Session()


output = session.run(fetches=y,feed_dict={
    x:[3.0]
})

print("Output is : "+str(output))

## note we can alose supply multiple inputs to the place holder node
## and it will compute y=Mx+c for all those values

##eg

output = session.run(fetches=y,feed_dict={
    x:[3.0,4.0,5.0,6.0]
})

print("Output is : "+str(output))



