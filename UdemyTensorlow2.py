import tensorflow as tf

#Variable nodes

# If we have a value for a node that will change then
# we will use variable nodes


'''

   Variable(
               initial_value=None,
               trainable=True,
               collections=None,
               validate_shape=True,
               caching_device=None,
               name=None,
               variable_def=None,
               dtype=None,
               expected_shape=None,
               import_scope=None,
               constraint=None,
               use_resource=None,
               synchronization=VariableSynchronization.AUTO,
               aggregation=VariableAggregation.NONE):
'''

var1 = tf.Variable(initial_value=[1.0],  # same as value part of Constant node but its value can change over time
                   trainable=True, #this means should we allow value to change during training phase (in training phase variable way change in order to minimize error func).If False then values can be changed only ma nually.
                   collections=None,   #(*)
                   validate_shape=True, # same as verify shape (checks the value given and Rows, Cloumns are matching )
                   caching_device=None,   #(*)
                   name="var_1", # name of node (helpful when saving model in form of a test file)
                   variable_def=None,   #(*)
                   dtype=tf.float32, # data type
                   expected_shape=(1,None), # this is out expected shape
                   import_scope= None  #(*)
                   )

# values whi9ch are highlightes as * are not that imp ryt now


# now to get the values of this variable node  we need a session
# it is important to note that in variable nodes,
# we need an extra step in order to run() a session

# we have to call session.run(tf.global_variables_initializer()) before running any session on variable nodes


#1)

session  = tf.Session()
model = tf.global_variables_initializer()
session.run(model)

# now after above steps we
# can now run session on our veriable nodes

output = session.run(fetches=var1)
print("Output of Var1 : "+str(output))


# now if we want to re assign a  value of variable node we will use assign operation

# pleas enote we have to consume the output of assign in order to see
# this is related to node operation
# which is covered in furture videos

var2 = var1.assign([2.0])

print("\nAfter Assignment..\n")

print("\nVar_1 : "+str(session.run(fetches=var1)))
print("\nVar_2 : "+str(session.run(fetches=var2)))

