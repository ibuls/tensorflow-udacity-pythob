import tensorflow as tf

## y= Wx + b note-> here W denotes Weights can be interchanged with M(same thing)



from numpy.core.multiarray import dtype

W = tf.Variable(initial_value=[1.0],dtype=tf.float32,name='W')
b = tf.Variable(initial_value=[1.0],dtype=tf.float32,name='b')

x = tf.placeholder(dtype=tf.float32,name='x')

# for protobuff file we alos must specify the name of each and every node

y_input = tf.placeholder(dtype=tf.float32,name='y_input')


# we can not use the below expression cos it is not suitable for protobuff file
# y_output = W * x + b

y_output = tf.add(x=tf.multiply(x = W,y=x,name='multiply'),y=b,name='y_output')

loss = tf.reduce_sum(input_tensor=tf.square(x=y_output-y_input),name='loss')

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.01,name='optimizer')

train_setp =  optimizer.minimize(loss=loss,name='train_step')

######## WRITING TO PROTOBUFF FILE #######

## there are mainly 3 steps involved

# 1) create saver object
saver = tf.train.Saver()

session = tf.Session()
session.run(tf.global_variables_initializer())

## creating the shape is actually done after creating session object


# 2)
tf.train.write_graph(graph_or_graph_def=session.graph_def,
                     logdir='.',
                     name='./linear_regression.pbtxt',
                     as_text=False ## false because we want a binary file not a text file.
                     )

## the above code has simply written the shape of the graph (written graph)

x_train = [1.0,2.0,3.0,4.0]
y_train  = [-1.0,-2.0,-3.0,-4.0]

out_before_training = session.run(loss,feed_dict={x:x_train,y_input:y_train})
print("before training output : "+str(out_before_training))
print("\n\n")


## training the graph (1000)
for _ in range(1000):
    session.run(fetches=train_setp,feed_dict={x:x_train,y_input:y_train})


# 3)
## (saved graph)
saver.save(sess=session,save_path='./linear_regression.ckpt')


out_after_training = session.run(fetches=[loss,W,b],feed_dict={x:x_train,y_input:y_train})
print("after training output : "+str(out_after_training))
print("\n\n")
testing_model = session.run(fetches=y_output,feed_dict={x:[5.0,10.0,15.0]})
print("Testing Model output : "+str(testing_model))






