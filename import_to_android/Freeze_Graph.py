import tensorflow as tf
from tensorflow.python.tools import freeze_graph,optimize_for_inference_lib



## this file is used for creating an optimized protobuff file which is very useful for android devices


## There are basically 3 steps to achive an optimized graph from our .pbtxt and .ckpt file

## 1) generate another file 'frozen_linear_regression.pb' (note only pb not pbtxt)
## 2) read the above file and save it in string format
## 3) gereate optimized 'optimized_frozen_linear_regression.pb' from the string generated in step 2.



### Please run LR_Model_To_Protobuff.py before running this file


## 1)

freeze_graph.freeze_graph(input_graph='./linear_regression.pbtxt', ## our pbtxt file
                          input_saver='',
                          input_binary=True, # we saved in binary hence true
                          input_checkpoint= './linear_regression.ckpt', ## out checkpoint file name
                           output_node_names='y_output', # out output node name
                          restore_op_name='save/restore_all', #operation nme to store
                          filename_tensor_name='save/Const:0', # we dont have so use constant
                          output_graph='./frozen_linear_regression.pb',  ## our optimized file name
                          clear_devices=True,
                          initializer_nodes='',
                          variable_names_blacklist='')


## 2)

input_graph_def = tf.GraphDef()
with tf.gfile.Open('./frozen_linear_regression.pb','rb') as f:
    data = f.read()
    input_graph_def.ParseFromString(data)


## at this point our input_graph_def contains all the data that is present in our 'frozen_linear_regression.pb' file in string format



## 3)

output_graph_def = optimize_for_inference_lib.optimize_for_inference(input_graph_def=input_graph_def,
                                                                     input_node_names=['x'],
                                                                     output_node_names=['y_output'],
                                                                     placeholder_type_enum=tf.float32.as_datatype_enum)




## now we have to write the Optimized Protobuff file to a file called 'optimized_frozen_linear_regression.pb'

f  = tf.gfile.FastGFile(name='./optimized_frozen_linear_regression.pb',mode='w')
f.write(file_content=output_graph_def.SerializeToString())

print("Data Written to file")