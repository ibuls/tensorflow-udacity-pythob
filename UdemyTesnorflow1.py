import tensorflow as tf

#Constans nodes

# If we have a value for a node that is not going to change then
# we will store that value in a constant node



const_1 = tf.constant(value=[[1.0,2.0],[2.2,3.3],[1.2,1.3]],  # value for the constant
                      dtype=tf.float32,     # actual type of the values in the list
                      shape=(3,2),          # shape tells the rows,cols of value
                      name="const1",        # name of the node
                      verify_shape=True     # verfify the values dimension and shape inputs match
                      )



# to see what values are currently present in the node
# we use sessions

session = tf.Session()

output = session.run(fetches=const_1)

print("Value obtained from node count_1 : \n"+str(output))

print("\n\n")

## similarly we can also create other constant nodes like cont_1


const_2 = tf.constant(value=[[6.0,5.0]],  # value for the constant
                      dtype=tf.float32,     # actual type of the values in the list
                      shape=(1,2),          # shape tells the rows,cols of value
                      name="const2",        # name of the node
                      verify_shape=True     # verfify the values dimension and shape inputs match
                      )


# now to get values of multi nodes simple specify the node objects in the fatches of run method

output = session.run(fetches=[const_1,const_2])

print("Value obtained from count_1 and const_2 : \n"+str(output))
